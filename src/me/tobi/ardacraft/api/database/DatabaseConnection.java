package me.tobi.ardacraft.api.database;

import java.io.File;
import java.sql.*;

public class DatabaseConnection {
	
	private Connection conn = null;
	
	public DatabaseConnection() {
		openDatabase();
	}

	public Connection getConnection() {
		return conn;
	}
	
	private void openDatabase() {
		try {
			File f = new File("ArdaCraft.db");
			if (!f.exists()) {
				throw new RuntimeException("FEHLER: Die Datenbank wurde nicht gefunden!");
			}
			Class.forName("org.sqlite.JDBC");
			conn = DriverManager.getConnection("jdbc:sqlite:ArdaCraft.db");
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("Opened database successfully");
	}
	
	public void closeDatabase() {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

    public void execute(String sql) {
		try {
			Statement stmt = conn.createStatement();
            stmt.executeUpdate(sql);
			stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
