package me.tobi.ardacraft.api.database;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.City;
import me.tobi.ardacraft.api.classes.User;
import me.tobi.ardacraft.api.classes.Utils;
import org.bukkit.Location;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CityManager {

    DatabaseConnection conn;

    public CityManager(DatabaseConnection connection) {
        conn = connection;
    }

    public boolean contains(String name) {
        return get(name) != null;
    }

    public List<City> getAll() {
        List<City> cities = new ArrayList<>();
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM CITIES;");
            while(result.next()) {
                int id = result.getInt("id");
                String name = result.getString("NAME");
                User owner = ArdaCraftAPI.getACDatabase().getUserManager().get(result.getString("OWNER_ID"));
                Location location = Utils.string2Location(result.getString("LOCATION"));
                boolean isGood = result.getBoolean("IS_GOOD");
                HashMap<User, Utils.Position> userPositions = getUserPositions(id);
                cities.add(City.construct(id, name, owner, location ,isGood,userPositions));
            }
            result.close();
            statement.close();
        }catch(SQLException ex) {ex.printStackTrace();}
        return cities;
    }

    public City get(int rowID) {
        int id = 0;
        String name = "";
        User owner = null;
        Location location = null;
        boolean isGood = false;
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM CITIES WHERE ID=" + rowID + ";");
            if(result.next()) {
                id = result.getInt("ID");
                name = result.getString("NAME");
                owner = ArdaCraftAPI.getACDatabase().getUserManager().get(result.getString("OWNER_ID"));
                location = Utils.string2Location(result.getString("LOCATION"));
                isGood = result.getBoolean("IS_GOOD");
                result.close();
                statement.close();
            }else {
                result.close();
                statement.close();
                return null;
            }
        }catch(SQLException ex) {ex.printStackTrace();}
        return City.construct(id, name, owner, location ,isGood, getUserPositions(id));
    }

    public City get(String cname) {
        int id = 0;
        String name = "";
        User owner = null;
        Location location = null;
        boolean isGood = false;
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM CITIES WHERE name='" + cname + "';");
            if(result.next()) {
                id = result.getInt("id");
                name = result.getString("NAME");
                owner = ArdaCraftAPI.getACDatabase().getUserManager().get(result.getString("OWNER_ID"));
                location = Utils.string2Location(result.getString("LOCATION"));
                isGood = result.getBoolean("IS_GOOD");
                result.close();
                statement.close();
            }else {
                result.close();
                statement.close();
                return null;
            }
        }catch(SQLException ex) {ex.printStackTrace();}
        return City.construct(id, name, owner, location ,isGood, getUserPositions(id));
    }

    public HashMap<User, Utils.Position> getUserPositions(int cityID) {
        HashMap<User, Utils.Position> userPosition = new HashMap<>();
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM CITY_has_USERS where CITY_ID=" + cityID + ";");
            while(result.next()) {
                User user = ArdaCraftAPI.getACDatabase().getUserManager().get(result.getString("USER_UUID"));
                Utils.Position position = Utils.Position.valueOf(result.getString("USER_POSITION"));
                userPosition.put(user, position);
            }
            result.close();
            statement.close();
        }catch(SQLException ex) {ex.printStackTrace();}
        return userPosition;
    }

    public void delete(String name) {
        delete(get(name).getId());
    }

    public void delete(int rowID) {
        conn.execute("DELETE FROM CITIES WHERE ID=" + rowID + ";");
        conn.execute("DELETE FROM CITY_has_USERS where CITY_ID=" + rowID + ";");
    }

    public void update(City city) {
        conn.execute("UPDATE CITIES SET "
                + "NAME='" + city.getDatabaseName() + "', "
                + "OWNER_ID='" + city.getOwner().getUUID() + "', "
                + "LOCATION='" + Utils.location2String(city.getLocation()) + "' "
                + "IS_GOOD='" + city.isGood() + "' "
                + "WHERE ID='" + city.getId() + "';");
        for(User u : city.getUsersWithPositions().keySet()) {
            conn.execute("UPDATE CITY_has_USERS SET "
                    + "USER_UUID='" + u.getUUID() + "', "
                    + "USER_POSITION='" + city.getUsersWithPositions().get(u).toString() + "' "
                    + "WHERE CITY_ID='" + city.getId() + "';");
        }
    }

    public void add(City city) {
        if(!contains(city.getDatabaseName())) {
            conn.execute("INSERT INTO CITIES (ID,NAME,OWNER_ID,LOCATION,IS_GOOD) VALUES (" +
                    "NULL, '" +
                    city.getDatabaseName() + "', '" +
                    city.getOwner().getUUID() + "', '" +
                    Utils.location2String(city.getLocation()) + "', '" +
                    city.isGood() + "');");
            if(city.getUsersWithPositions() != null) {
                for (User u : city.getUsersWithPositions().keySet()) {
                    conn.execute("INSERT INTO CITY_has_USERS (USER_UUID, CITY_ID, USER_POSITION) VALUES ('" +
                            u.getUUID() + "'," +
                            city.getId() + ", '" +
                            city.getUsersWithPositions().get(u).toString() + "');");
                }
            }
        }else {
            System.err.println("City already exists!");
        }
    }

}