package me.tobi.ardacraft.api.database;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CharacterManager {

    DatabaseConnection conn;

    public CharacterManager(DatabaseConnection connection) {
        conn = connection;
    }

    public boolean contains(String name) {
        return get(name) != null;
    }

    public List<Charakter> getAll() {
        List<Charakter> charakters = new ArrayList<>();
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM USERS;");
            while(result.next()) {
                charakters.add(new Charakter(result.getInt("ID"), result.getString("NAME"),
                        Rasse.valueOf(result.getString("VOLK")), result.getBoolean("IS_MAIN")));
            }
            result.close();
            statement.close();
        }catch(SQLException ex) {ex.printStackTrace();}
        return charakters;
    }

    public Charakter get(int rowID) {
        int id = -1;
        String name = "";
        Rasse volk = null;
        boolean isMain = false;
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM CHARACTERS WHERE ID=" + rowID + ";");
            if(result.next()) {
                id = result.getInt("ID");
                name = result.getString("NAME");
                volk = Rasse.valueOf(result.getString("VOLK"));
                isMain = result.getBoolean("IS_MAIN");
                result.close();
                statement.close();
            }else {
                result.close();
                statement.close();
                return null;
            }
        }catch(SQLException ex) {ex.printStackTrace();}
        return new Charakter(id, name, volk, isMain);
    }

    public Charakter get(String cname) {
        int id = -1;
        String name = "";
        Rasse volk = null;
        boolean isMain = false;
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM CHARACTERS WHERE NAME='" + cname + "';");
            if(result.next()) {
                id = result.getInt("ID");
                name = result.getString("NAME");
                volk = Rasse.valueOf(result.getString("VOLK"));
                isMain = result.getBoolean("IS_MAIN");
                result.close();
                statement.close();
            }else {
                result.close();
                statement.close();
                return null;
            }
        }catch(SQLException ex) {ex.printStackTrace();}
        return new Charakter(id, name, volk, isMain);
    }

    public List<Charakter> get(Rasse rasse) {
        List<Charakter> charaktere = new ArrayList<>();
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM CHARACTERS WHERE VOLK='" + rasse.toString() + "';");
            while(result.next()) {
                charaktere.add(new Charakter(result.getInt("ID"), result.getString("NAME"), Rasse.valueOf(result.getString("VOLK")),
                        result.getBoolean("IS_MAIN")));
            }
            result.close();
            statement.close();

        }catch(SQLException ex) {ex.printStackTrace();}
        return charaktere;
    }

    public void delete(int rowID) {
        conn.execute("DELETE FROM CHARACTERS WHERE ID=" + rowID + ";");
    }

    public void delete(String name) {
        conn.execute("DELETE FROM CHARACTERS WHERE name='" + name + "';");
    }

    public void update(Charakter charakter) {
        conn.execute("UPDATE CITIES SET "
                + "NAME='" + charakter.getName() + "', "
                + "VOLK='" + charakter.getRasse() + "', "
                + "IS_MAIN='" + charakter.isMain() + "' "
                + "WHERE ID='" + charakter.getId() + "';");
    }

    public void add(Charakter charakter) {
        if(!contains(charakter.getName())) {
            conn.execute("INSERT INTO CHARACTERS (ID,NAME,VOLK,IS_MAIN) VALUES (" +
                    "NULL, '" +
                    charakter.getName() + "', '" +
                    charakter.getRasse().toString() + "', '" +
                    charakter.isMain() + "');");
        }else {
            System.err.println("Character already exists!");
        }
    }

}