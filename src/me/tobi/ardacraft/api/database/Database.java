package me.tobi.ardacraft.api.database;

public class Database {

    private DatabaseConnection connection;
    private UserManager userManager;
    private CharacterManager characterManager;
    private CityManager cityManager;
    private RegionManager regionManager;

    public Database() {
        connection = new DatabaseConnection();
        userManager = new UserManager(connection);
        characterManager = new CharacterManager(connection);
        cityManager = new CityManager(connection);
        regionManager = new RegionManager(connection);
    }

    public DatabaseConnection getConnection() {
        return connection;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public CharacterManager getCharacterManager() {
        return characterManager;
    }

    public CityManager getCityManager() {
        return cityManager;
    }

    public RegionManager getRegionManager() {
        return regionManager;
    }

}
