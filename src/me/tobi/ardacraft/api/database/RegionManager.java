package me.tobi.ardacraft.api.database;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.classes.City;
import me.tobi.ardacraft.api.classes.Region;
import me.tobi.ardacraft.api.classes.Utils;
import org.bukkit.Location;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class RegionManager {

    DatabaseConnection conn;

    public RegionManager(DatabaseConnection connection) {
        conn = connection;
    }

    public boolean contains(String name) {
        return get(name) != null;
    }

    public List<Region> getAll() {
        List<Region> regions = new ArrayList<>();
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM REGIONS");
            while(result.next()) {
                regions.add(Region.construct(result.getInt("ID"), ArdaCraftAPI.getACDatabase().getCityManager().get(result.getString("CITY_ID")),
                        result.getString("NAME"), Utils.string2Location(result.getString("LOCATION")), result.getInt("CP_STATE")));
            }
            result.close();
            statement.close();
        }catch (SQLException ex) { ex.printStackTrace(); }
        return regions;
    }

    public Region get(int rowID) {
        int id = 0;
        String name = "";
        City city = null;
        Location location = null;
        int cpState = 0;
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM REGIONS WHERE ID=" + rowID + ";");
            if(result.next()) {
                id = result.getInt("ID");
                name = result.getString("NAME");
                city = ArdaCraftAPI.getACDatabase().getCityManager().get(result.getString("CITY_ID"));
                location = Utils.string2Location(result.getString("LOCATION"));
                cpState = result.getInt("CP_STATE");
                result.close();
                statement.close();
            }else {
                result.close();
                statement.close();
                return null;
            }
        }catch(SQLException ex) {ex.printStackTrace();}
        return Region.construct(id, city, name, location, cpState);
    }

    public Region get(String rname) {
        int id = 0;
        String name = "";
        City city = null;
        Location location = null;
        int cpState = 0;
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM REGIONS WHERE NAME='" + rname + "';");
            if(result.next()) {
                id = result.getInt("ID");
                name = result.getString("NAME");
                city = ArdaCraftAPI.getACDatabase().getCityManager().get(result.getString("CITY_ID"));
                location = Utils.string2Location(result.getString("LOCATION"));
                cpState = result.getInt("CP_STATE");
                result.close();
                statement.close();
            }else {
                result.close();
                statement.close();
                return null;
            }
        }catch(SQLException ex) {ex.printStackTrace();}
        return Region.construct(id, city, name, location, cpState);
    }

    public List<Region> get(City ccity) {
        List<Region> regions = new ArrayList<>();
        try {
            Statement statement = conn.getConnection().createStatement();
            ResultSet result = statement.executeQuery("SELECT * FROM REGIONS WHERE CITY_ID='" + ccity.getId() + "';");
            while(result.next()) {
                int id = result.getInt("ID");
                String name = result.getString("NAME");
                City city = ArdaCraftAPI.getACDatabase().getCityManager().get(result.getString("CITY_ID"));
                Location location = Utils.string2Location(result.getString("LOCATION"));
                int cpState = result.getInt("CP_STATE");
                regions.add(Region.construct(id, city, name, location, cpState));
            }
            result.close();
            statement.close();
        }catch(SQLException ex) {ex.printStackTrace();}
        return regions;
    }

    public void delete(int rowID) {
        conn.execute("DELETE FROM REGIONS WHERE ID=" + rowID + ";");
    }
    //Name is not unique

    public void update(Region region) {
        conn.execute("UPDATE REGIONS SET "
                + "NAME='" + region.getName() + "', "
                + "CITY_ID='" + region.getCity().getId() + "', "
                + "LOCATION='" + region.getLocation() + "' "
                + "CP_STATE='" + region.getCpState() + "' "
                + "WHERE ID='" + region.getId() + "';");
    }

    public void add(Region region) {
        if(!contains(region.getName())) {
            conn.execute("INSERT INTO REGIONS (ID,NAME,CITY_ID,LOCATION,CP_STATE) VALUES (" +
                    "NULL, '" +
                    region.getName()+ "', " +
                    region.getCity().getId() + ", '" +
                    Utils.location2String(region.getLocation()) + "', " +
                    region.getCpState() + ");");
        }else {
            System.err.println("Region already exists!");
        }
    }

}