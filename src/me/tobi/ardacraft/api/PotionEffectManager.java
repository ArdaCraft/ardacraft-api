package me.tobi.ardacraft.api;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionEffectManager {
	
	public static void addPotionEffect(Player p, PotionEffect pe) {
		if(p.hasPotionEffect(pe.getType())) {
			if(getAmplifier(p, pe.getType()) >= pe.getAmplifier()) {
				return;
			}else {
				p.removePotionEffect(pe.getType());
				p.addPotionEffect(pe);
			}
		}else {
			p.addPotionEffect(pe);
		}
	}
	
	public static int getAmplifier(Player p, PotionEffectType type) {
		for(PotionEffect pe : p.getActivePotionEffects()) {
			if(pe.getType().equals(type)) {
				return pe.getAmplifier();
			}
		}
		return -1;
	}
	
}
