package me.tobi.ardacraft.api;

import me.tobi.ardacraft.api.classes.Logger;
import me.tobi.ardacraft.api.database.Database;
import org.bukkit.plugin.java.JavaPlugin;

public class ArdaCraftAPI extends JavaPlugin {
	
	private static Database db;
	private static ArdaCraftAPI plugin;
	private static Logger logger;
	@Override
	public void onEnable() {
		//TEST
		plugin = this;
		db = new Database();
		//db.open();
		logger = new Logger();
		//if(db.getCity("Spawn") == null) {
		//	City c = new City("Spawn", "NOONE", new Location(getServer().getWorld("world"), 1, 2, 3), Attitude.NEUTRAL);
		//	db.addCity(c);
		//}
		/*System.out.println("ENABLING ===========================");
		City c = new City("my-CITY1", "my-UUID3", new Location(this.getServer().getWorld("world"), 11, 21, 31), Attitude.BAD);
		db.addCity(c);
		db.addUser(new User("my-UUID1", c, Rasse.ELB, "Nickname1", 10));
		db.addUser(new User("my-UUID2", c, Rasse.HOBBIT, "Nickname2", 20));
		db.addUser(new User("my-UUID3", c, Rasse.MAGIER, "Nickname3", 30));
		db.addUser(new User("my-UUID4", c, Rasse.WARGREITER, "Nickname4", 40));
		System.out.println(db.getUser("my-UUID1").getLevel());
		System.out.println(db.getUser("my-UUID2").getRasse());
		System.out.println(db.getUser("my-UUID3").getUUID());
		System.out.println(db.getUser("my-UUID4").getCity().getName());
		//db.addCity(c);
		//db.addCity(new City("my-CITY2", "my-UUID4", new Location(this.getServer().getWorld("world"), 12, 22, 32), Attitude.GOOD));
		//System.out.println(db.getCity("my-CITY1").getLocation());
		//System.out.println(db.getCity("my-CITY2").getAttitude().toString());
		/*
		System.out.println("TEST 2 ===");
		User u = new User("my-UUID5", c, Rasse.MENSCH, "Menschenskind", 5);
		db.addUser(u);
		System.out.println("lvl: " + db.getUser("my-UUID5").getLevel());
		u.setLevel(70);
		db.updateUser(u);
		System.out.println("lvl: " + db.getUser("my-UUID5").getLevel());
		System.out.println("TEST 3 ===");
		City c2 = new City("my-CITY3", "my-UUID2", new Location(this.getServer().getWorld("world"), 13, 23, 33), Attitude.NEUTRAL);
		db.addCity(c2);
		System.out.println("owner: " + db.getCity("my-CITY3").getOwnerUUID());
		c2.setOwnerUUID("my-UUID1");
		db.updateCity(c2);
		System.out.println("owner: " + db.getCity("my-CITY3").getOwnerUUID());
		System.out.println("ENABLING ===========================");*/
	}
	
	@Override
	public void onDisable() {
		db.getConnection().closeDatabase();
	}
	
	public static ArdaCraftAPI getPlugin() {
		return plugin;
	}
	
	public static Database getACDatabase() {
		return db;
	}
	
	public static Logger getACLogger() {
		return logger;
	}

}