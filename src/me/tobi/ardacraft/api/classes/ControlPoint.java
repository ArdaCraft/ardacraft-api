package me.tobi.ardacraft.api.classes;

import org.bukkit.Location;

@Deprecated
public class ControlPoint {
	
	private Location loc;
	private City city;
	private Integer state;
	private String name;
	
	public ControlPoint(Location loc, City c, Integer state, String name) {
		setLocation(loc);
		setCity(c);
		setState(state);
		setName(name);
	}

	public Location getLocation() {
		return loc;
	}

	public void setLocation(Location loc) {
		this.loc = loc;
	}

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}
