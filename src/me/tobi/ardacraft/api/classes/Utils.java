package me.tobi.ardacraft.api.classes;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import me.tobi.ardacraft.api.PotionEffectManager;
import me.tobi.ardacraft.api.json.JSONArray;
import me.tobi.ardacraft.api.json.JSONObject;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class Utils {
	
	/**
	 * @param p 
	 * Player
	 * @return
	 * eine String Liste mit allen Namen, die der Spieler hatte
	 */
	public static  List<String> getNames(Player p) {
		String names = null;
		Scanner scanner;
		String uuid = p.getUniqueId().toString().replaceAll("-", "");
		try {
			scanner = new Scanner(new URL("https://api.mojang.com/user/profiles/" + uuid + "/names").openStream());
			names = scanner.nextLine();
	        scanner.close();
		} catch (IOException e) {}
		JSONArray arr = new JSONArray(names);
		List<String> namen = new ArrayList<String>();
		if (arr != null) {
			int length = arr.length();
			for (int i = 0; i < length; i++) {
				JSONObject obj = new JSONObject(arr.get(i).toString());
				if(!obj.has("name")) {
					break;
				}
				namen.add(obj.getString("name").toString());
			}
		}
		return namen;
	}
	
	public static void sendJsonMessage(String msg, Player p) {
		IChatBaseComponent comp = IChatBaseComponent.ChatSerializer.a(msg);
		PacketPlayOutChat packet = new PacketPlayOutChat(comp);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
	}
	
	@SuppressWarnings("deprecation")
	public static double getDamageFor(Player attacker, Entity attacked, int damage, boolean byProjectile) {
		double damageRTN = damage;
		// crits
		if (attacker.getVelocity().getY() < 0 && !attacker.isOnGround() &&          // ATTACK
				!attacker.getLocation().getBlock().getType().equals(Material.LADDER)
				&& !attacker.getLocation().getBlock().getType().equals(Material.VINE)) {
			damageRTN = damageRTN * 1.5;
			// multiply by 150%
		}
		
		//STRENGTH
		if(attacker.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE)) {
			int addByStrength = PotionEffectManager.getAmplifier(attacker, PotionEffectType.INCREASE_DAMAGE) * 3;
			damageRTN = damageRTN + addByStrength + 3;
		}
		
		//Blocking
		if (attacked instanceof HumanEntity) { // DEFENSE
			HumanEntity attackedHuman = (HumanEntity) attacked;
			if (attackedHuman.isBlocking()) {
				damageRTN = damageRTN * 0.5;
				// multiply by 50%
			}
		}
		
		//Armor
		if(attacked instanceof HumanEntity) {          //DEFENSE
			HumanEntity he = (HumanEntity)attacked;
			int armorPoints = getArmorPoints(he);
			double armor = armorPoints * 0.01 * 4; //from 0 to 0.8
			int aep = getArmorEnchantmentPoints(he, byProjectile);
			double enchantment = aep * 0.01 * 4; //from 0 to 0.8
			double rest = 1 - armor; //from 0 to 0.2
			double enchantmentRest = rest * enchantment; //from 0 to 0.16
			double together = (armor + enchantmentRest) *0.5; //from 0 to 0.465
			//System.out.println("faktor = " + together);
			//System.out.println("reducing damage by "  + (damageRTN * together));
			damageRTN = damageRTN - (damageRTN * together);
		}
		//Aufrunden
		damageRTN = Math.ceil(damageRTN);
		return damageRTN;
	}
	
	@SuppressWarnings("deprecation")
	private static int getArmorEnchantmentPoints(HumanEntity he, boolean byProjectile) {
		int ePP = 0; //enchantmentProtectionPoints
		//HELMET
		if(he.getInventory().getHelmet() != null) {
			Map<Enchantment, Integer> enchantmentsHelmet = he.getInventory().getHelmet().getEnchantments();
			if(enchantmentsHelmet != null) {
				for(Enchantment e : enchantmentsHelmet.keySet()) {
					if(e.getId() == 0) {
						ePP = ePP + enchantmentsHelmet.get(e);
						if(enchantmentsHelmet.get(e) == 4) {ePP++;}
					}
					if(byProjectile && e.getId() == 4) {
						int level = enchantmentsHelmet.get(e);
						if(level == 1) {ePP = ePP + 3;}
						if(level == 2) {ePP = ePP + 5;}
						if(level == 3) {ePP = ePP + 7;}
						if(level == 4) {ePP = ePP + 11;}
					}
				}
			}
		}
		//CHESTPLATE
		if(he.getInventory().getChestplate() != null) {
			Map<Enchantment, Integer> enchantmentsChest = he.getInventory().getChestplate().getEnchantments();
			if(enchantmentsChest != null) {
				for(Enchantment e : enchantmentsChest.keySet()) {
					if(e.getId() == 0) {
						ePP = ePP + enchantmentsChest.get(e);
						if(enchantmentsChest.get(e) == 4) {ePP++;}
					}
					if(byProjectile && e.getId() == 4) {
						int level = enchantmentsChest.get(e);
						if(level == 1) {ePP = ePP + 3;}
						if(level == 2) {ePP = ePP + 5;}
						if(level == 3) {ePP = ePP + 7;}
						if(level == 4) {ePP = ePP + 11;}
					}
				}
			}
		}
		//LEGGINGS
		if(he.getInventory().getLeggings() != null) {
			Map<Enchantment, Integer> enchantmentsLeggings = he.getInventory().getLeggings().getEnchantments();
			if(enchantmentsLeggings != null) {
				for(Enchantment e : enchantmentsLeggings.keySet()) {
					if(e.getId() == 0) {
						ePP = ePP + enchantmentsLeggings.get(e);
						if(enchantmentsLeggings.get(e) == 4) {ePP++;}
					}
					if(byProjectile && e.getId() == 4) {
						int level = enchantmentsLeggings.get(e);
						if(level == 1) {ePP = ePP + 3;}
						if(level == 2) {ePP = ePP + 5;}
						if(level == 3) {ePP = ePP + 7;}
						if(level == 4) {ePP = ePP + 11;}
					}
				}
			}
		}
		//BOOTS
		if(he.getInventory().getBoots() != null) {
			Map<Enchantment, Integer> enchantmentsBoots = he.getInventory().getBoots().getEnchantments();
			if(enchantmentsBoots != null) {
				for(Enchantment e : enchantmentsBoots.keySet()) {
					if(e.getId() == 0) {
						ePP = ePP + enchantmentsBoots.get(e);
						if(enchantmentsBoots.get(e) == 4) {ePP++;}
					}
					if(byProjectile && e.getId() == 4) {
						int level = enchantmentsBoots.get(e);
						if(level == 1) {ePP = ePP + 3;}
						if(level == 2) {ePP = ePP + 5;}
						if(level == 3) {ePP = ePP + 7;}
						if(level == 4) {ePP = ePP + 11;}
					}
				}
			}
		}
		if(ePP > 25) {
			ePP = 25;
		}
		//bis zu halbieren
		ePP = ePP - (int)Math.ceil(ePP * Math.random() / 2);
		
		if(ePP > 20) {
			ePP = 20;
		}
		return ePP;
	}
	
	private static int getArmorPoints(HumanEntity he) {
		int rtn = 0;
		//HELMET
		if(he.getInventory().getHelmet() != null) {
			if(he.getInventory().getHelmet().getType() == Material.LEATHER_HELMET) {
				rtn = rtn + 1;
			}else if(he.getInventory().getHelmet().getType() == Material.GOLD_HELMET || 
					he.getInventory().getHelmet().getType() == Material.CHAINMAIL_HELMET ||
					he.getInventory().getHelmet().getType() == Material.IRON_HELMET) {
				rtn = rtn + 2;
			}else if(he.getInventory().getHelmet().getType() == Material.DIAMOND_HELMET) {
				rtn = rtn + 3;
			}
		}
		//CHEST
		if(he.getInventory().getChestplate() != null) {
			if(he.getInventory().getChestplate().getType() == Material.LEATHER_CHESTPLATE) {
				rtn = rtn + 3;
			}else if(he.getInventory().getChestplate().getType() == Material.GOLD_CHESTPLATE ||
					he.getInventory().getChestplate().getType() == Material.CHAINMAIL_CHESTPLATE) {
				rtn = rtn + 5;
			}else if(he.getInventory().getChestplate().getType() == Material.IRON_CHESTPLATE) {
				rtn = rtn + 6;
			}else if(he.getInventory().getChestplate().getType() == Material.DIAMOND_CHESTPLATE) {
				rtn = rtn + 8;
			}
		}
		//LEGGINGS
		if(he.getInventory().getLeggings() != null) {
			if(he.getInventory().getLeggings().getType() == Material.LEATHER_LEGGINGS) {
				rtn = rtn + 2;
			}else if(he.getInventory().getLeggings().getType() == Material.GOLD_LEGGINGS) {
				rtn = rtn + 3;
			}else if(he.getInventory().getLeggings().getType() == Material.CHAINMAIL_LEGGINGS) {
				rtn = rtn + 4;
			}else if(he.getInventory().getLeggings().getType() == Material.IRON_LEGGINGS) {
				rtn = rtn + 5;
			}else if(he.getInventory().getLeggings().getType() == Material.DIAMOND_LEGGINGS) {
				rtn = rtn + 6;
			}
		}
		//BOOTS
		if(he.getInventory().getBoots() != null) {
			if(he.getInventory().getBoots().getType() == Material.LEATHER_BOOTS ||
					he.getInventory().getBoots().getType() == Material.GOLD_BOOTS ||
					he.getInventory().getBoots().getType() == Material.CHAINMAIL_BOOTS ) {
				rtn = rtn + 1;
			}else if(he.getInventory().getBoots().getType() == Material.IRON_BOOTS) {
				rtn = rtn + 2;
			}else if(he.getInventory().getBoots().getType() == Material.DIAMOND_BOOTS) {
				rtn = rtn + 3;
			}
		}
		return rtn;
	}
	
	/**
	 * 
	 * @param i
	 * Integer Array
	 * @return
	 * den kleinsten Wert der Liste
	 */
	public static int min(int[] i) {
		int ii = 0;
		for(int in = 0; in < i.length; in++) {
		   if(i[in] < ii) {
		      ii = i[in];
		   }
		}		
		return ii;
	}
	
	/**
	 * 
	 * @param m
	 * LinkedHashMap
	 * @return
	 * den kelinsten double Wert aus der Liste
	 */
	public static double min(LinkedHashMap<Double, String> m) {		
		double t = Integer.MAX_VALUE;
		for (Double d : m.keySet()) {
			if(d < t) {
				t = d;
			}
		}		
		return t;
	}
	
	
	/**
	 * 
	 * @param entity
	 * @return
	 * einen double Wert f�r den R�stungs Modifier
	 */
	public static double getDamageReduced(HumanEntity entity)
    {
		double reduced = 0.0;
		try {
	        PlayerInventory inv = entity.getInventory();
	        ItemStack boots = inv.getBoots();
	        ItemStack helmet = inv.getHelmet();
	        ItemStack chest = inv.getChestplate();
	        ItemStack pants = inv.getLeggings();
	        
	        if(helmet.getType() == Material.LEATHER_HELMET)reduced = reduced + 0.04;
	        else if(helmet.getType() == Material.GOLD_HELMET)reduced = reduced + 0.08;
	        else if(helmet.getType() == Material.CHAINMAIL_HELMET)reduced = reduced + 0.08;
	        else if(helmet.getType() == Material.IRON_HELMET)reduced = reduced + 0.08;
	        else if(helmet.getType() == Material.DIAMOND_HELMET)reduced = reduced + 0.12;
	        if(boots.getType() == Material.LEATHER_BOOTS)reduced = reduced + 0.04;
	        else if(boots.getType() == Material.GOLD_BOOTS)reduced = reduced + 0.04;
	        else if(boots.getType() == Material.CHAINMAIL_BOOTS)reduced = reduced + 0.04;
	        else if(boots.getType() == Material.IRON_BOOTS)reduced = reduced + 0.08;
	        else if(boots.getType() == Material.DIAMOND_BOOTS)reduced = reduced + 0.12;
	        if(pants.getType() == Material.LEATHER_LEGGINGS)reduced = reduced + 0.08;
	        else if(pants.getType() == Material.GOLD_LEGGINGS)reduced = reduced + 0.12;
	        else if(pants.getType() == Material.CHAINMAIL_LEGGINGS)reduced = reduced + 0.16;
	        else if(pants.getType() == Material.IRON_LEGGINGS)reduced = reduced + 0.20;
	        else if(pants.getType() == Material.DIAMOND_LEGGINGS)reduced = reduced + 0.24;
	        if(chest.getType() == Material.LEATHER_CHESTPLATE)reduced = reduced + 0.12;
	        else if(chest.getType() == Material.GOLD_CHESTPLATE)reduced = reduced + 0.20;
	        else if(chest.getType() == Material.CHAINMAIL_CHESTPLATE)reduced = reduced + 0.20;
	        else if(chest.getType() == Material.IRON_CHESTPLATE)reduced = reduced + 0.24;
	        else if(chest.getType() == Material.DIAMOND_CHESTPLATE)reduced = reduced + 0.32;
		}catch(Exception e) {}
        return reduced;
    }
	
	/**
	 * 
	 * @param message String
	 * @return
	 * einen Normalisierten String, "Xxxxxx"
	 */
	public static String normalize(String message){
	    char newChar = message.charAt(0);
	    newChar = java.lang.Character.toUpperCase(newChar);
	    message = message.toLowerCase();
	    message = message.replaceFirst(String.valueOf(message.charAt(0)),String.valueOf(newChar));			
	    return message;
	}
	
	/**
	 * Löscht die Effekte des Spielers p
	 * @param p 
	 * Player
	 */
	public static void clearEffects(Player p) {
		for (PotionEffect effect : p.getActivePotionEffects()){
	        p.removePotionEffect(effect.getType());
		}
	}
	/**
	 * Gibt den Namen der n�chsten Stadt von der Position l aus zur�ck
	 * @param l Location 
	 * @return String name der n�chst gelegenen Stadt
	 */
	public static String getNearestCity(Location l) {
		double min = Integer.MAX_VALUE;
		String city = "";
		for(City c : ArdaCraftAPI.getACDatabase().getCityManager().getAll()) {
			Location loc = c.getLocation();
			double distance = l.distance(loc);
			if(min > distance) {
				min = distance;
				city = c.getDisplayName();
			}
		}
		return city + "(" + (int)min + "m)";
	}	
	
	public static Region getNearestRegion(Location l) {
		double min = Integer.MAX_VALUE;
		Region region = null;
		for(Region r : ArdaCraftAPI.getACDatabase().getRegionManager().getAll()) {
			Location loccp = r.getLocation();
			if(loccp.getWorld().equals(l.getWorld())) {
				double dist = loccp.distance(l);
				if(min > dist) {
					min = dist;
					region = r;
				}
			}
		}
		return region;
	}
	
	
	/**
	 * 
	 * @param obj Object Array
	 * @return Einen zufälligen Eintrag aus dem Array
	 */
	public static Object pickRandomOf(Object[] obj) {
		Random rnd = new Random();
		if(obj.length == 0) {
			return null;
		}
		return obj[rnd.nextInt(obj.length)];
	}
	
	/**
	 * Gibt Alle Spieler in einem Radius von <b>radius</b> um die Position <b>loc</b> zur�ck.
	 * @param loc Location
	 * @param radius Integer 
	 * @return Player Array
	 */
	public static Player[] getPlayersAround(Location loc, int radius) {
		int counter = 0;
		for(Player pl : Bukkit.getServer().getOnlinePlayers()) {
			if(loc.getWorld() == pl.getWorld()) {
				double dist = loc.distance(pl.getLocation());
				if(dist < 10){
					if(!(dist < 1))
					counter += 1;
				}
			}
		}
		int c2 = 0;
		Player[] pa = new Player[counter];
		for(Player pl : Bukkit.getServer().getOnlinePlayers()) {
			if(loc.getWorld() == pl.getWorld()) {
				double dist = loc.distance(pl.getLocation());
				if(dist < 10){
					if(!(dist < 1)) {
						pa[c2] = pl;
						c2 += 1;
					}
				}
			}
		}
		return pa;
	}
	
	/**
	 * 
	 * @param loc Location
	 * @param radius Integer
	 * @return Nächstgelegener Spieler um die Position <b>loc</b> in einem Radius von <b>radius</b> Bl�cken
	 */
	public static Player getNearestPlayer(Location loc, int radius) {
		double min = Integer.MAX_VALUE;
		Player nearest = null;
		for(Player p : getPlayersAround(loc, radius)) {
			double dist = loc.distance(p.getLocation());
			if(dist < min) {
				min = dist;
				nearest = p;
			}
		}
		return nearest;
	}
	
	public static boolean removeOne(Material item, Player from) {
		if(!from.getInventory().contains(item)) {
			return false;
		}
		for (int i = 0; i < from.getInventory().getSize(); i++) {
			ItemStack itm = from.getInventory().getItem(i);
			if (itm != null && itm.getType().equals(item)) {
				int amt = itm.getAmount() - 1;
				itm.setAmount(amt);
				from.getInventory().setItem(i, amt > 0 ? itm : null);
				from.updateInventory();
				break;
			}
		}
		return true;
	}
	
	public static void removeOne(ItemStack item, Player from) {
		for (int i = 0; i < from.getInventory().getSize(); i++) {
			ItemStack itm = from.getInventory().getItem(i);
			if (itm != null && itm.equals(item)) {
				int amt = itm.getAmount() - 1;
				itm.setAmount(amt);
				from.getInventory().setItem(i, amt > 0 ? itm : null);
				from.updateInventory();
				break;
			}
		}
	}

	public static Object getPrivateField(String fieldName, Class clazz, Object object) {
		Field field;
		Object o = null;
		try	{
			field = clazz.getDeclaredField(fieldName);
			field.setAccessible(true);
			o = field.get(object);
		}
		catch(NoSuchFieldException e) {
			e.printStackTrace();
		}
		catch(IllegalAccessException e) {
			e.printStackTrace();
		}
		return o;
	}

	public static void mark(Location loc, int expandX, int expandZ, byte color) {
		int xOffset = expandX;
		int zOffset = expandZ;
		int yValue = loc.getBlockY();
		double xBase = loc.getBlockX()+0.5;
		double zBase = loc.getBlockZ()+0.5;
		List<Location> blocks = new ArrayList<>();
		for(int i = 0; i < xOffset; i++) {
			blocks.add(new Location(loc.getWorld(), xBase + i, yValue, zBase));
			blocks.add(new Location(loc.getWorld(), xBase + i, yValue, zBase+zOffset));
		}
		for(int i = 0; i < zOffset; i++) {
			blocks.add(new Location(loc.getWorld(), xBase, yValue, zBase + i));
			blocks.add(new Location(loc.getWorld(), xBase + xOffset, yValue, zBase + i));
		}
		blocks.add(new Location(loc.getWorld(), xBase + xOffset, yValue, zBase + zOffset));
		for(Location loca : blocks) {
			FallingBlock fb = loc.getWorld().spawnFallingBlock(loca, Material.STAINED_GLASS, (byte)color);
			fb.setVelocity(fb.getVelocity().multiply(0));
			fb.setDropItem(false);
			fb.setHurtEntities(false);
			fb.setInvulnerable(true);
			fb.setGravity(false);
		}
	}

	public static String location2String(Location loc) {
		String rtn = loc.getWorld().getName() + "\t" + loc.getX() + "\t" + loc.getY() + "\t" + loc.getZ();
		return rtn;
	}

	public static Location string2Location(String str) {
		String[] strs = str.split("\t");
		World w = Bukkit.getServer().getWorld(strs[0]);
		Double x = Double.valueOf(strs[1]);
		Double y = Double.valueOf(strs[2]);
		Double z = Double.valueOf(strs[3]);
		Location rtn = new Location(w, x, y, z);
		return rtn;
	}

	/** Just*/
	public static Location getRandomLocation(Location nearOf, int range, World world) {

		int x = randomInt(nearOf.getBlockX() - range, nearOf.getBlockX() + range);
		int z = randomInt(nearOf.getBlockZ() - range, nearOf.getBlockZ() + range);
		int y = world.getHighestBlockYAt(x, z) + 1;
		return world.getBlockAt(x, y + 1, z).getLocation();
	}

	/** min and max inclusive */
	public static Integer randomInt(int min, int max){
		return (min + (int)(Math.random() * ((max - min) + 1)));
	}
	
	/**
	 * Enum der verschiedenen Gesinnungen
	 */
	public enum Attitude {
		GOOD, BAD, NEUTRAL, UNKNOWN
	}

	public enum Position {
		ADMIN, MEMBER, ACQUINTANCE, NEUTRAL, BANNED
	}
}
