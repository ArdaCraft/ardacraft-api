package me.tobi.ardacraft.api.classes;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import org.bukkit.Bukkit;

import java.util.Arrays;

public class User {
	
	private	String UUID;
	private Charakter charakter;
	private String lastName;
	private long dateCharacterChanged;
	private long lastSeen;
	
	public User(String UUID, Charakter c, String lastName, long dateCharacterChanged, long lastSeen) {
		setUUID(UUID);
		setCharakter(c);
		setLastName(lastName);
		setDateCharacterChanged(dateCharacterChanged);
		setLastSeen(lastSeen);
	}


	public String getUUID() {
		return UUID;
	}


	public void setUUID(String UUID) {
		this.UUID = UUID;
	}

	public Charakter getCharakter() {
		return charakter;
	}

	public void setCharakter(Charakter charakter) {
		this.charakter = charakter;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getDateCharacterChanged() {
		return dateCharacterChanged;
	}

	public void setDateCharacterChanged(long dateCharakterChanged) {
		this.dateCharacterChanged = dateCharakterChanged;
	}

	public long getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(long lastSeen) {
		this.lastSeen = lastSeen;
	}





	public static boolean isValid(String name) {
		try {
			if(ArdaCraftAPI.getACDatabase().getUserManager().get(Bukkit.getPlayer(name).getUniqueId().toString()) == null) {
				return false;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	
	public static User getByName(String name) {
		UUIDFetcher fetcher = new UUIDFetcher(Arrays.asList(name));
		try {
			return ArdaCraftAPI.getACDatabase().getUserManager().get(fetcher.call().get(name).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static User getByUUID(String uuid) {
		return ArdaCraftAPI.getACDatabase().getUserManager().get(uuid);
	}
	
	/*public static List<User> getUsersInCity(City c) {
		List<User> rtn = new ArrayList<User>();
		for(User u : ArdaCraftAPI.getACDatabase().getUsersWithPositions()) {
			if(u.getCity().getName().equalsIgnoreCase(c.getName())) {
				rtn.add(u);
			}
		}
		return rtn;
	}
	
	public static User getByNick(String nick) {
		for(User u : ArdaCraftAPI.getACDatabase().getUsersWithPositions()) {
			if(u.getPlayername() == nick) {
				return u;
			}
		}
		return null;
	}
	
	public List<City> getCities() {
		List<City> rtn = new ArrayList<City>();
		for(City c : ArdaCraftAPI.getACDatabase().getCities()) {
			if(User.getByUUID(c.getOwnerUUID()).getCity().getName().equalsIgnoreCase(c.getName())) {
				rtn.add(c);
			}
		}
		return rtn;
	}*/
	
}
