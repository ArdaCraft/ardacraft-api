package me.tobi.ardacraft.api.classes;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import org.bukkit.entity.Player;

public class Charakter {
	private int id;
	private String name;
	private Rasse rasse;
	private Boolean isMain;
	
	public Charakter(int id, String name, Rasse rasse, Boolean isMain) {
		setName(name);
		setRasse(rasse);
		setMain(isMain);
		setId(id);
	}
	
	public static Charakter get(Player p) {
		return ArdaCraftAPI.getACDatabase().getUserManager().get(p.getUniqueId().toString()).getCharakter();
	}
	
	public boolean isTaken() {
		for(User u : ArdaCraftAPI.getACDatabase().getUserManager().getAll()) {
			if(u.getCharakter() != null) {
				if(u.getCharakter().getName().equalsIgnoreCase(name)) {
					return true;
				}
			}
		}
		return false;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Rasse getRasse() {
		return rasse;
	}

	public void setRasse(Rasse rasse) {
		this.rasse = rasse;
	}

	public Boolean isMain() {
		return isMain;
	}

	public void setMain(Boolean isMain) {
		this.isMain = isMain;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}


	/*public String getPlayerUUID() {
		return playerUUID;
	}

	public void setPlayerUUID(String playerUUID) {
		this.playerUUID = playerUUID;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date givenAt) {
		this.date = givenAt;
	}*/
	
}
