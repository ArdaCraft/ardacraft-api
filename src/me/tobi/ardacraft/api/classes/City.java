package me.tobi.ardacraft.api.classes;

import me.tobi.ardacraft.api.ArdaCraftAPI;
import org.bukkit.Location;

import java.util.HashMap;
import java.util.List;

public class City {

	private int id;
	private String name;
	private User owner;
	private Location location;
	private boolean isGood;
	private HashMap<User, Utils.Position> userPositions;
	
	private City() {}

    public static City construct(int id, String name, User owner, Location loc, boolean isGood, HashMap<User, Utils.Position> userPositions) {
        City c = new City();
        c.setId(id);
        c.setName(name);
        c.setOwner(owner);
        c.setLocation(loc);
        c.setGood(isGood);
        c.setUsersWithPositions(userPositions);
        return c;
    }

	public static City create(String name, User owner, Location loc, boolean isGood, HashMap<User, Utils.Position> userPositions) {
		City c = new City();
		c.setId(-1);
		c.setName(name);
		c.setOwner(owner);
		c.setLocation(loc);
		c.setGood(isGood);
		c.setUsersWithPositions(userPositions);
        ArdaCraftAPI.getACDatabase().getCityManager().add(c);

		//loading the city from the DB to get the cities ID
        Region.create(ArdaCraftAPI.getACDatabase().getCityManager().get(name), "Zentrum", loc);
		return c;
	}

    public Region getRegion(Location loc) {
        for(Region r : getRegions()) {
            if(r.contains(loc)) {
                return r;
            }
        }
        return null;
    }

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public boolean isGood() {
		return isGood;
	}

	public void setGood(boolean good) {
		isGood = good;
	}

	public HashMap<User, Utils.Position> getUsersWithPositions() {
		return userPositions;
	}

	public void setUsersWithPositions(HashMap<User, Utils.Position> userPositions) {
		this.userPositions = userPositions;
	}

	public String getDatabaseName() {
		return name;
	}

	public String getDisplayName() { return toDisplayName(getDatabaseName()); }

	/**
	 * sets the name for the DB
	 * @param name
     */
	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

    public void addRegion(Region region) {

    }


	public static String toDatabaseName(String name) {
		name = name.replaceAll(" ", "_");
		name = name.toLowerCase();
		return name;
	}

	public static String toDisplayName(String name) {
		char[] chars = name.toCharArray();
		chars[0] = Character.toUpperCase(chars[0]);
		boolean toUpp = false;
		for(int i = 0; i < name.length(); i++) {
			if(toUpp) {
				chars[i] = Character.toUpperCase(chars[i]);
				toUpp = false;
			}
			if(chars[i] == '_') {
				chars[i] = ' ';
				toUpp = true;
			}
		}
		return String.valueOf(chars);
	}

	public List<Region> getRegions() {
		return ArdaCraftAPI.getACDatabase().getRegionManager().get(this);
	}
	
}
