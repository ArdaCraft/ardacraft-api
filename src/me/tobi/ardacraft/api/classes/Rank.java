package me.tobi.ardacraft.api.classes;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;

public enum Rank {
	CMOD{
		@Override
		public List<String> getPermissions() {
			List<String> permissions = new ArrayList<String>();
			permissions.add("ardacraft.team.cmod");
			permissions.add("essentials.warp.*");
			permissions.add("essentials.warp");
			permissions.add("essentials.warps.*");
			permissions.add("essentials.warps");
			permissions.add("mb.*");
			permissions.add("essentials.ban");
			permissions.add("simplewarnings.*");
			permissions.add("essentials.unban");
			return permissions;
		}
		@Override
		public boolean isHigherThen(Rank r) {
			if(r == Rank.SPIELER) {
				return true;
			}else {
				return false;
			}
		}
	}, MOD{
		@Override
		public List<String> getPermissions() {
			List<String> permissions = new ArrayList<String>();
			permissions.add("ardacraft.team.mod");
			permissions.add("essentials.*");
			permissions.add("mb.*");
			permissions.add("simplewarnings.*");
			return permissions;
		}
		@Override
		public boolean isHigherThen(Rank r) {
			if(r == Rank.ADMIN || r == Rank.MOD) {
				return false;				
			}else {
				return true;
			}
		}
	}, ADMIN{
		@Override
		public List<String> getPermissions() {
			List<String> permissions = new ArrayList<String>();
			permissions.add("*");
			return permissions;
		}
		@Override
		public boolean isHigherThen(Rank r) {
			return true;
		}
	}, SPIELER{
		@Override
		public List<String> getPermissions() {
			List<String> permissions = new ArrayList<String>();
			permissions.add("essentials.msg");
			return permissions;
		}
		@Override
		public boolean isHigherThen(Rank r) {
			return false;
		}
	};
	
	/**
	 * @return <b>String List</b> Die Permissions des Rangs
	 */
	public List<String> getPermissions() {
		return null;
	}
	
	/**
	 * @param p Player
	 * @return <b>Rank</b> Den Rang des Spielers
	 */
	public static Rank get(Player p) {
		PermissionUser user = PermissionsEx.getUser(p);
		if(p.getUniqueId().toString().equals("ae99dd1d-3bd9-4d45-800b-b9676901d823") || p.getUniqueId().toString().equals("a4d4dfcf-1622-452c-9022-c80d445c68b7")) {
			return Rank.ADMIN;
		}
		if(user.getSuffix().contains("cmod")) {
			return Rank.CMOD;
		}
		if(user.getSuffix().contains("mod")) {
			return Rank.MOD;
		}
		if(user.getSuffix() == null) {
			return Rank.SPIELER;
		}
		return Rank.SPIELER;
	}
	
	public static String getPrefix(Player p) {
		if(Rank.get(p) == Rank.ADMIN) {
			return "§cADM§r";
		}else if(Rank.get(p) == Rank.MOD) {
			return "§bMOD§r";
		}else if(Rank.get(p) == Rank.CMOD) {
			return "§9CMOD§r";
		}else {
			return "§fSPI§r";
		}
	}
	
	public boolean isHigherThen(Rank r) {
		return false;
	}
	
}
