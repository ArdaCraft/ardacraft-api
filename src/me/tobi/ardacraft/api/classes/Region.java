package me.tobi.ardacraft.api.classes;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldguard.bukkit.WGBukkit;
import com.sk89q.worldguard.domains.DefaultDomain;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.flags.StateFlag;
import com.sk89q.worldguard.protection.regions.ProtectedCuboidRegion;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import me.tobi.ardacraft.api.ArdaCraftAPI;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.UUID;

public class Region {

    private int id;
    private String name;
    private City city;
    private Location location;
    private int cpState;

    private Region() {}

    public static Region construct(int id, City city, String name, Location loc, int cpState) {
        Region r = new Region();
        r.setId(id);
        r.setName(name);
        r.setCity(city);
        r.setLocation(loc);
        r.setCpState(cpState);
        return r;
    }

    public static Region create(City city, String name, Location location) {
        Region r = new Region();
        r.setId(-1);
        r.setName(name);
        r.setCity(city);
        r.setLocation(location);
        r.setCpState(0);
        ArdaCraftAPI.getACDatabase().getRegionManager().add(r);

        Region added = ArdaCraftAPI.getACDatabase().getRegionManager().get(name);
        Location minLoc = location.getBlock().getLocation().add(-25, -1*location.getBlockY(), -25);
        BlockVector min = new BlockVector(minLoc.getBlockX(), minLoc.getBlockY(), minLoc.getBlockZ());
        Location maxLoc = location.getBlock().getLocation().add(25, 255 - (-1*location.getBlockY()), 25);
        BlockVector max = new BlockVector(maxLoc.getBlockX(), maxLoc.getBlockY(), maxLoc.getBlockZ());
        ProtectedRegion region = new ProtectedCuboidRegion("ACREGION_" + city.getId() + "-" + added.getId(), min, max);
        region.setFlag(DefaultFlag.BLOCK_BREAK, StateFlag.State.DENY);
        region.setFlag(DefaultFlag.BUILD, StateFlag.State.DENY);
        region.setFlag(DefaultFlag.CHEST_ACCESS, StateFlag.State.DENY);
        region.setFlag(DefaultFlag.DAMAGE_ANIMALS, StateFlag.State.DENY);

        DefaultDomain dd = new DefaultDomain();
        dd.addPlayer(UUID.fromString(city.getOwner().getUUID()));

        region.setOwners(dd);

        Utils.mark(minLoc.getBlock().getLocation(), 51, 51, (byte)5);

        try {
            com.sk89q.worldguard.protection.managers.RegionManager manager = WGBukkit.getPlugin().getRegionContainer().get(Bukkit.getWorld("RPG"));
            manager.addRegion(region);
            manager.save();
        }catch (Exception ex) {ex.printStackTrace();}
        return r;
    }

    public static void remove(int id) {
        Region r = ArdaCraftAPI.getACDatabase().getRegionManager().get(id);
        try {
            com.sk89q.worldguard.protection.managers.RegionManager manager = WGBukkit.getPlugin().getRegionContainer().get(Bukkit.getWorld("RPG"));
            manager.removeRegion("ACREGION_" + r.getCity().getId() + "-" + r.getId());
            manager.save();
        }catch (Exception ex) {ex.printStackTrace();}
        ArdaCraftAPI.getACDatabase().getRegionManager().delete(id);
    }

    /**
     * überprüft für location ob eine neue Region möglich wäre
     */
    public static boolean isValid(City city, String name, Location location) {
        Region region = Region.construct(-1, city, name, location, 0);
        if(!ArdaCraftAPI.getACDatabase().getRegionManager().contains(name)) {
            if(city.getRegion(location) == null) {
                if(getRelativeRegionForNewRegionFrom(location, city) != null) {
                    return true;
                }else {
                    return false;
                }
            }else {
                return false;
            }
        }else {
            return false;
        }
    }

    /**
     * gibt die region zurück, von der aus die neue Region erstellt werden kann.
     * das ist die region, der der spieler am nächsten ist, wobei er nicht diagonal stehen darf
     */
    private static Region getRelativeRegionForNewRegionFrom(Location loc, City c) {
        Region x1 = c.getRegion(loc.getBlock().getRelative(51, 0, 0).getLocation());
        Region x2 = c.getRegion(loc.getBlock().getRelative(-51, 0, 0).getLocation());
        Region z1 = c.getRegion(loc.getBlock().getRelative(0, 0, 51).getLocation());
        Region z2 = c.getRegion(loc.getBlock().getRelative(0, 0, -51).getLocation());
        double dist1 = Integer.MAX_VALUE;
        double dist2 = Integer.MAX_VALUE;
        double dist3 = Integer.MAX_VALUE;
        double dist4 = Integer.MAX_VALUE;
        if(x1 != null) dist1 = loc.distance(x1.getLocation());
        if(x2 != null) dist2 = loc.distance(x2.getLocation());
        if(z1 != null) dist3 = loc.distance(z1.getLocation());
        if(z2 != null) dist4 = loc.distance(z2.getLocation());
        if(dist1<=dist2 && dist1<=dist3 && dist1<=dist4) {
            return x1;
        }else if(dist2<dist1 && dist2<dist3 && dist2<dist4) {
            return x2;
        }else if(dist3<dist1 && dist3<dist2 && dist3<dist4) {
            return z1;
        }else if(dist4<dist1 && dist4<dist2 && dist4<dist3) {
            return z2;
        }else if(dist1==dist2 && dist2==dist3 && dist3==dist4) {
            return null;
        }
        //
        throw new RuntimeException("Keine gültige Region gefunden...");
    }

    public static Location getNewRegionMidLocation(Location loc, City c) {
        if(isValid(c, "kein.name", loc)) {
            Region relative = getRelativeRegionForNewRegionFrom(loc, c);
            Region x1 = c.getRegion(loc.getBlock().getRelative(51, 0, 0).getLocation());
            Region x2 = c.getRegion(loc.getBlock().getRelative(-51, 0, 0).getLocation());
            Region z1 = c.getRegion(loc.getBlock().getRelative(0, 0, 51).getLocation());
            Region z2 = c.getRegion(loc.getBlock().getRelative(0, 0, -51).getLocation());

            Location mid = null;
            if(x1.getName().equals(relative.getName())) {
                mid = x1.getLocation().getBlock().getRelative(-51, 0, 0).getLocation();
            }else if(x2.getName().equals(relative.getName())) {
                mid = x2.getLocation().getBlock().getRelative(51, 0, 0).getLocation();
            }else if(z1.getName().equals(relative.getName())) {
                mid = z1.getLocation().getBlock().getRelative(0, 0, -51).getLocation();
            }else if(z2.getName().equals(relative.getName())) {
                mid = z2.getLocation().getBlock().getRelative(0, 0, 51).getLocation();
            }
            return mid;
        }else {
            return null;
        }
    }

    /*public boolean cutsAcross(Region r) {
        Location aDL = getLocation().getBlock().getRelative(-25, 0, -25).getLocation(); //DownLeft
        Location aUR = getLocation().getBlock().getRelative(25, 0, 25).getLocation(); //UpRight

        Location bDL = r.getLocation().getBlock().getRelative(-25, 0, -25).getLocation(); //DownLeft
        Location bUR = r.getLocation().getBlock().getRelative(25, 0, 25).getLocation(); //UpRight

        boolean fits = bDL.getX() > aUR.getX() && bDL.getZ() > aUR.getZ() || bUR.getX() < aDL.getX() && bUR.getZ() < aDL.getZ();
    }*/

    public boolean contains(Location loc) {
        Location DL = getLocation().getBlock().getRelative(-25, 0, -25).getLocation(); //DownLeft
        Location UR = getLocation().getBlock().getRelative(25, 0, 25).getLocation(); //UpRight
        return loc.getX() >= DL.getX() && loc.getX() <= UR.getX()
                && loc.getZ() >= DL.getZ() && loc.getZ() <= UR.getZ();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public int getCpState() {
        return cpState;
    }

    public void setCpState(int cpState) {
        this.cpState = cpState;
    }

    public ProtectedRegion getWorldGuardRegion() {
        return WGBukkit.getPlugin().getRegionManager(Bukkit.getWorld("RPG")).getRegion("ACREGION_" + id);
    }

    /*public Border[] getBorders() {
        Border[] borders = new Border[4];
        borders[0] = getBorder(BorderSide.NORTH);
        borders[1] = getBorder(BorderSide.EAST);
        borders[2] = getBorder(BorderSide.SOUTH);
        borders[3] = getBorder(BorderSide.WEST);
        return borders;
    }

    public Border getBorder(BorderSide side) {
        if(side == BorderSide.NORTH) {
            Location start = getLocation().getBlock().getRelative(-25, 0, 25).getLocation();
            return new Border(start, Border.Orientation.HORIZONTALLY, 51);
        }else if(side == BorderSide.WEST) {
            Location start = getLocation().getBlock().getRelative(-25, 0, -25).getLocation();
            return new Border(start, Border.Orientation.VERTICALLY, 51);
        }else if(side == BorderSide.SOUTH) {
            Location start = getLocation().getBlock().getRelative(-25, 0, -25).getLocation();
            return new Border(start, Border.Orientation.HORIZONTALLY, 51);
        }else if(side == BorderSide.EAST) {
            Location start = getLocation().getBlock().getRelative(25, 0, -25).getLocation();
            return new Border(start, Border.Orientation.VERTICALLY, 51);
        }
        throw new RuntimeException("invalid BorderSide");
    }

    public enum BorderSide {
        NORTH, WEST, SOUTH, EAST
      // +Z     -X     -Z    +X
    }

    public static class Border {
        Location beginning;
        int length;
        Orientation orientation;

        public Border(Location startPoint, Orientation orientation, int length) {
            setBeginning(startPoint);
            setLength(length);
            setOrientation(orientation);
        }

        public boolean isParallel(Border border) {
            if(border.getOrientation() == getOrientation()) {
                if(getBeginning().getBlock().getLocation().distance(border.getBeginning().getBlock().getLocation()) == 1) {
                    return true;
                }else {
                    return true;
                }
            }else {
                return false;
            }
        }

        public Location getBeginning() {
            return beginning;
        }

        public void setBeginning(Location beginning) {
            this.beginning = beginning;
        }

        public int getLength() {
            return length;
        }

        public void setLength(int length) {
            this.length = length;
        }

        public Orientation getOrientation() {
            return orientation;
        }

        public void setOrientation(Orientation orientation) {
            this.orientation = orientation;
        }

        public enum Orientation {
            HORIZONTALLY, VERTICALLY
            // +X            +Z
        }

    }*/


}
